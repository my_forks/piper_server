CREATE DATABASE piper_db;
CREATE USER piper_user WITH PASSWORD 'piper_pass';
ALTER ROLE piper_user SET client_encoding TO 'utf8';
ALTER ROLE piper_user SET default_transaction_isolation TO 'read committed';
ALTER ROLE piper_user SET timezone TO 'UTC';
ALTER USER piper_user CREATEDB;
GRANT ALL PRIVILEGES ON DATABASE piper_db TO piper_user;
\q
