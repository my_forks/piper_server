# Piper Server
Piper Server is the web application component of the Piper Pipeline Toolkit - an out-of-the-box CGI production tracking 
and data management software suite.

Developed and maintained by [Viktor Petrov](https://gitlab.com/vikmpetrov), Piper I/O EOOD.

## Version
1.0.1

## Pre-requisites
To install Piper Server, additional software is required.
Depending on the system, these might need to be installed manually.

### On Windows:
Microsoft C++ Build Tools are required to build the Python module 'mod-wsgi' from source.
* Download [Microsoft C++ Build Tools](https://visualstudio.microsoft.com/visual-cpp-build-tools/)
* Install the C++ Build Tools

### On Linux:
Some packages must be installed before executing the setup script.

Note that these are the requirements on Ubuntu 20.04. There might be some differences on other Linux distributions.

To download the packages required by the installation:
* curl

To build the packages from source:
* gcc
* make

To build Python:
* minimal build:
    * zlib1g-dev
    * libffi-dev
* optional modules (should be installed):
    * libbz2-dev (bz2)
    * libncurses5-dev (curses)
    * libgdbm-dev (gdbm)
    * liblzma-dev (lzma)
    * libsqlite3-dev (sqlite3)
    * libssl-dev (ssl + hashlib)
    * tk-dev (tkinter + uuid)
    * libreadline-dev (readline)

To build Apache:
* libpcre3-dev (pcre)
* apache2-dev ('expat' header)

## Installation & configuration
To install this software, simply place this package anywhere you deem appropriate on your server.

### 1. Setup
Installing Python and Apache, and setting up a virtual environment.

#### On Windows:
Navigate to this directory and run the following script:
```
.\setup.bat
```
Or just double-click on the file run the script.

#### On Linux:
Open a terminal shell, navigate to this directory and run the following script:
```
source setup.sh
```

#### On Mac:
Open a terminal shell, navigate to this directory and run the following script:
```
source setup.command
```

### 2. Setting up a PostgreSQL database
Prerequisite: download and install [PostgreSQL](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads).

Note that on Linux you can also install PostgreSQL using a package manager ('postgresql' and 'postgresql-client' packages).

Note that some installations will require you to set a server-wide PostgreSQL password, which you should remember for later use.

#### On Windows:
(Optional) Save your PostgreSQL password in your pgpass.conf file, e.g. "C:\Users\username\AppData\Roaming\postgresql\pgpass.conf":

```
localhost:5432:*:postgres:password
127.0.0.1:5432:*:postgres:password
```

Find the location of the SQL Shell (psql) (located in the 'bin' directory of the PostgreSQL install).

Open a terminal shell, navigate to the Piper Server setup directory and run the following command:
```
<PSQL_PATH>\psql.exe -U postgres -f db_init.sql
```
where '<PSQL_PATH>' is the path to the SQL Shell (e.g.: "C:\Program Files\PostgreSQL\13\bin")

#### On Linux:
Open a terminal shell and run the following commands:
```
sudo -su postgres
psql -f db_init.sql
exit
```

#### On Mac:
Open a terminal shell and run the following commands:
```
pg_ctl -D /usr/local/var/postgres start && brew services start postgresql
psql -U postgres -f db_init.sql
```

### 3. Allowing access to server
Before running the server, it is required to add the server in the configuration.
In the directory 'piper_server_app' of the server install location, modify the file 'settings.py' by adding the server's machine IP and/or hostname in the allowed hosts.
E.g.:
```
ALLOWED_HOSTS = [
  '<server_name>',
  '127.0.0.1',
  'localhost',
  '0.0.0.0'
]
```
where <server_name> is the IP address or the hostname of the server.

## Deployment
After executing the run script (see below for each OS), the Piper Server can be reach at the following URL:
```
http://<server_name>:8080
```
where <server_name> is the IP address or the hostname of the server.

### On Windows:
Open a Windows Power shell, navigate to this directory and run the following command:
```
.\run.bat
```

### On Linux:
Open a terminal shell, navigate to this directory and run the following command:
```
source run.sh
```

### On Mac:
Open a terminal shell, navigate to this directory and run the following command:
```
source run.command
```

## License

This software package's EULA is outlined in its [LICENSE.md](LICENSE.md) file.

## Copyright
&copy; Piper I/O EOOD. All rights reserved.
