var task_link_id = null;

$( document ).ready(function() {
    $('.selectpicker').selectpicker();
    $('.select2-container').select2();
    $('#context-selector').addClass('panel-featured panel-featured-primary');
});

$('#project-select').on('change', function() {
    clear_calendar()
});

$('#episode-select').on('change', function() {
    clear_calendar()
});

function clear_calendar() {
    var $calendar = $('#calendar');
    $calendar.fullCalendar( 'removeEvents' );
    $calendar.fullCalendar( 'refetchEvents' )
    $calendar.fullCalendar( 'rerenderEvents' )
}

$('#asset-select').on('change', function() {
    $('#id_asset_id').val(this.value);
    task_link_id = this.value;
    $('#id_shot_id').val('');
    return get_link_tasks('Asset', this.value)
});

$('#shot-select').on('change', function() {
    $('#id_shot_id').val(this.value);
    task_link_id = this.value;
    $('#id_asset_id').val('');
    return get_link_tasks('Shot', this.value)
});

function task_update_callback() {
    if (task_link_id) {
        level = get_pipeline_level();
        return get_link_tasks(level, task_link_id)
    }
}

$('.context-selector-tab').on('classChanged', function() {
    level = get_pipeline_level();
    var id;
    if (level == 'Asset') {
        id = $('#shot-select').val();
        level = 'Shot';
    } else {
        id = $('#asset-select').val();
        level = 'Asset';
    }
    if (id) {
        return get_link_tasks(level, id)
    }
});

function get_link_tasks(type, id) {
    var $calendar = $('#calendar');
    $calendar.fullCalendar( 'removeEvents' );
    $.ajax({
        url: '/api/' + type + '/' + id + '/Task/?fields=artists_list,step,start_date,end_date,description',
        type: 'GET',
        success: function(json) {
                var tasks = []
                var start_year;
                var start_month;
                var start_day;
                var end_year;
                var end_month;
                var end_day;
                $.each(json, function( i, task ) {
                    start_year = parseInt(task.start_date.substring(0, 4))
                    start_month = parseInt(task.start_date.substring(5, 7))-1
                    start_day = parseInt(task.start_date.substring(8, 10))
                    end_year = parseInt(task.end_date.substring(0, 4))
                    end_month = parseInt(task.end_date.substring(5, 7))-1
                    end_day = parseInt(task.end_date.substring(8, 10))
                    tasks.push(
                    {
                        id: task.id,
                        title: task.step.long_name + ' | ' + task.long_name + ' | ' + task.artists_list.join(', ') + ' | ' + task.description,
                        start: new Date(start_year, start_month, start_day),
                        end: new Date(end_year, end_month, end_day),
                        allDay: true
				    })
                });
                if (tasks) {
                    $calendar.fullCalendar( 'addEventSource', tasks )
                }
            },
        error : update_error
    }).done(function() {
        $calendar.fullCalendar( 'refetchEvents' )
        $calendar.fullCalendar( 'rerenderEvents' )
    });
}

(function( func ) {
    $.fn.addClass = function() { // replace the existing function on $.fn
        func.apply( this, arguments ); // invoke the original function
        this.trigger('classChanged'); // trigger the custom event
        return this; // retain jQuery chainability
    }
})($.fn.addClass); // pass the original function as an argument

function re_time_task(event) {
    $.ajax({
        url: '/Task/' + event.id + '/re-time/' + event.start + '/' + event.end + '/',
        type: 'POST',
        data: {
            'start_time': moment(event.start).format("DD-MM-YYYY HH:mm"),
            'end_time': moment(event.end).format("DD-MM-YYYY HH:mm"),
            csrfmiddlewaretoken: getCookie('csrftoken')
            },
        success: update_success,
        error: update_error
    });
    return false;
}

var initCalendar = function(events) {
    var $calendar = $('#calendar');
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $calendar.fullCalendar({
        header: {
            left: 'title',
            right: 'prev,today,next,basicDay,basicWeek,month'
        },

        timeFormat: 'h:mm',
        firstDay: 1,
        selectable: true,
        selectHelper: true,
        editable: true,

        titleFormat: {
            month: 'MMMM YYYY',      // September 2009
            week: "MMM d YYYY",      // Sep 13 2009
            day: 'dddd, MMM d, YYYY' // Tuesday, Sep 8, 2009
        },

        themeButtonIcons: {
            prev: 'fa fa-caret-left',
            next: 'fa fa-caret-right',
        },

        select: function(start, end, allDay) {
                if ((context_shot_id) || (context_asset_id)) {
                    var level = get_pipeline_level();
                    $('#id__parent_model').val(level);
                    if (level == 'Asset') {
                        $('#id__parent_id').val(context_asset_id);
                    } else {
                        $('#id__parent_id').val(context_shot_id);
                    }
                    $('#id_start_date').val(start.toISOString());
                    $('#id_end_date').val(end.toISOString());
                    $('#createFormTrigger').trigger('click');
                    $('#calendar').fullCalendar('unselect');
				}
			},
        eventClick: function(calEvent, jsEvent, view) {
            refresh_edit_form('/Task/'+calEvent.id);
        },
        eventDrop: function(event, jsEvent, ui, view) {
            re_time_task(event);
        },
        eventResize: function(event, jsEvent, ui, view) {
            re_time_task(event);
        },
        events: events
    });

    // FIX INPUTS TO BOOTSTRAP VERSIONS
    var $calendarButtons = $calendar.find('.fc-header-right > span');
    $calendarButtons
        .filter('.fc-button-prev, .fc-button-today, .fc-button-next')
            .wrapAll('<div class="btn-group mt-sm mr-md mb-sm ml-sm"></div>')
            .parent()
            .after('<br class="hidden"/>');

    $calendarButtons
        .not('.fc-button-prev, .fc-button-today, .fc-button-next')
            .wrapAll('<div class="btn-group mb-sm mt-sm"></div>');

    $calendarButtons
        .attr({ 'class': 'btn btn-sm btn-default' });
};

(function( $ ) {
	'use strict';
	$(function() {
		initCalendar([]);
	});
}).apply(this, [ jQuery ]);